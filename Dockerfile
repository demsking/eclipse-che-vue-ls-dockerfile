# inherit an image with node.js and npm - runtime requirements for Vue LS
FROM eclipse/node

# install socat
RUN sudo apt-get install socat -y && \
    sudo npm install -g vue-language-server

# run socat that listens on port 4417 with exec command that starts LS
CMD socat TCP4-LISTEN:4417,reuseaddr,fork EXEC:"vls --stdio"
